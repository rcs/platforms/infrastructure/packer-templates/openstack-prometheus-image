# Openstack Prometheus Image

Openstack image for Prometheus


# Let's get going
You'll need a few bits to get this working.


# Configuring Env for work


clone repo

`git clone ...`


Create venv and install packages

`pipenv --python 3.7.6`

`pipenv sync`



Create ~/.config/openstack/clouds.yaml and populate with credentials

```
  arcus-bot:
    auth:
      auth_url: https://arcus.openstack.hpc.cam.ac.uk:5000/v3
      application_credential_id: "id_here"
      application_credential_secret: "secret_here"
    region_name: "RegionOne"
    interface: "public"
    identity_api_version: 3
    auth_type: "v3applicationcredential"

```

Test you can authenticate and use openstack APIs

`openstack image list --os-cloud=arcus-bot`


# Build all the things

`make prepare`

`make terraform`

`make build`


