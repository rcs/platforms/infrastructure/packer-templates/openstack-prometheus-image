output "packer_network_id" {
  value       = openstack_networking_network_v2.packer-internal-net.id
  description = "Packer network ID"
}

output "floating_ip_id" {
  value = openstack_networking_floatingip_v2.fip.id
  description = "packer floating IP ID"
}

output "floating_ip" {
  value = openstack_networking_floatingip_v2.fip.address
  description = "packer floating IP"
}