terraform {
  backend "swift" {
    container         = "prometheus-terraform-state"
    archive_container = "prometheus-state-backup"
  }
}

