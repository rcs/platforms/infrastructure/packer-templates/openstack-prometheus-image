locals {
  # name_prefix     = "${local.name_prefix}-vpc"
  name_prefix = "prometheus"
  cidr        = "10.10.0.0/16"

  networks = "${merge(
    map("internal", cidrsubnet(local.cidr, 8, 30)),
  )}"

  ssh_cidr = [
    "0.0.0.0/0",
  ]
}


resource "local_file" "packer-variables" {
  content = templatefile("./templates/vars.tmpl", {
    network_id  = openstack_networking_network_v2.packer-internal-net.id
    floating_ip_id = openstack_networking_floatingip_v2.fip.id
    floating_ip = openstack_networking_floatingip_v2.fip.address
    }
  )
  filename = "../env/vars.sh"
}

# Network
resource "openstack_networking_network_v2" "packer-internal-net" {
  name = "${local.name_prefix}-internal-net"

  # value_specs    = "${map("provider:network_type","vlan")}"
  admin_state_up = "true"
}

# Subnetworks
resource "openstack_networking_subnet_v2" "packer-internal-subnet" {
  name            = "${local.name_prefix}-internal-subnet"
  network_id      = openstack_networking_network_v2.packer-internal-net.id
  cidr            = lookup(local.networks, "internal")
  ip_version      = 4
  dns_nameservers  = var.dns_nameservers
}

resource "openstack_networking_router_v2" "packer-CUDN-Private" {
  name                = "${local.name_prefix}-CUDN-Private"
  admin_state_up      = "true"
  external_network_id = var.public_network
}

resource "openstack_networking_router_interface_v2" "packer-CUDN-Private" {
  router_id  = openstack_networking_router_v2.packer-CUDN-Private.id
  subnet_id  = openstack_networking_subnet_v2.packer-internal-subnet.id
  depends_on = [openstack_networking_router_v2.packer-CUDN-Private]
}

resource "openstack_networking_secgroup_v2" "packer-sec-grp" {
  name        = "${local.name_prefix}-packer-sec-grp"
  description = "packer ssh group"
}

resource "openstack_networking_secgroup_rule_v2" "packer_ssh_management" {
  count             = local.ssh_cidr == true ? 0 : 1
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = local.ssh_cidr[count.index]
  security_group_id = openstack_networking_secgroup_v2.packer-sec-grp.id
}

resource "openstack_networking_floatingip_v2" "fip" {
  description = "Floating IP for packer build"
  pool = "CUDN-Private"
}

resource "openstack_compute_keypair_v2" "packer-keypair" {
  name = "${local.name_prefix}-keypair"
  public_key = "${file("../packer-base.pub")}"
}