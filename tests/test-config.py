from time import sleep

import os
import uuid
import tempfile

import pytest
import testinfra


def test_ntp_is_installed(host):
    ntp = host.package("ntp")
    assert ntp.is_installed


def test_ntp_running_and_enabled(host):
    ntp = host.service("ntpd")

    assert ntp.is_running
    assert ntp.is_enabled


def check_banner_exists(host):
    f = host.file("/etc/motd")

    assert f.exists
    assert f.user == "root"
    assert f.group == "root"
    assert f.mode == 0o755

def check_prometheus_binary(host):
    f = host.file("usr/local/bin/prometheus")

    assert f.exists

@pytest.mark.parametrize(
    "name",
    [
        ("prometheus"),
        ("node_exporter"),
    ],
)
def test_services_running_and_enabled(host, name):
    service = host.service(name)
    assert service.is_running
    assert service.is_enabled

