.PHONY: help prepare clean validate build terraform

TEMPLATE_NAME=prometheus

help:
	@echo "Hi there!"
	@echo "Let's build a Image for ${TEMPLATE_NAME} in Openstack"
	@echo ""
	@echo "Our options are ::"
	@echo "prepare:       grabs Ansible roles.
	@echo "terraform:     creates essential openstack infra."
	@echo "validate:      validate the packer template"
	@echo "build:         build the image"
	@echo "clean:         remove the ./ansible/roles directory"

# Check requirements
ifeq (, $(shell which packer))
    $(error Error! packer command not in PATH)
endif
ifeq (, $(shell which terraform))
    $(error Error! terraform command not in PATH)
endif
ifeq (, $(shell which openstack))
    $(error Error! openstack command not in PATH. Check readme to set this up)
endif

prepare:
	@ansible-galaxy install -c -r ansible/requirements.yml --roles-path ./ansible/roles --force
	@ssh-keygen -t rsa -b 4096 -f packer-base -N ''

validate:
ifeq ($(CI_RUNNING),"FALSE")
	@echo "Removing FIP from known hosts..."
	@ssh-keygen -f "/home/${USER}/.ssh/known_hosts" -R "${FLOATING_IP}"
endif
	@packer validate templates/${TEMPLATE_NAME}.json

build: validate
	@PACKER_LOG=1 packer build -on-error=ask templates/${TEMPLATE_NAME}.json

clean:
	@rm -rf ansible/roles/

terraform:
	$(MAKE) -C terraform init
	$(MAKE) -C terraform plan
	$(MAKE) -C terraform apply

destroy:
	$(MAKE) -C terraform destroy
output:
	$(MAKE) -C terraform output
